using System;
using UnityEngine;
using UnityEngine.InputSystem;
using Utilities;

namespace Player
{
    public class PlayerController : MonoBehaviour
    {
        public float moveSpeed;
        public float maxSpeed;
        
        private Rigidbody _rb;
        private MeshCollider _mc;

        // Start is called before the first frame update
        private void Start()
        {
            _rb = GetComponent<Rigidbody>();
            _mc = GetComponent<MeshCollider>();
        }

        // Update is called once per frame
        private void Update()
        {
        
        }

        private void FixedUpdate()
        {
            var velocity = _rb.velocity;
            if (velocity.magnitude <= maxSpeed) return;
            var speedRatio = maxSpeed / velocity.magnitude;
            var newScaledVel = velocity.normalized * speedRatio;
            velocity.Scale(newScaledVel);
        }
        
        /// <summary>
        /// Picks up Move actions from the Input System
        /// will attempt to shepherd the context to whichever
        /// private method handles the appropriate movement Binding.
        /// TODO: FIX THIS
        /// </summary>
        /// <param name="ctx"></param>
        public void Move(InputAction.CallbackContext ctx)
        {
            var vec = MoveUtilities.Map2DControlVectorTo3D(ctx.ReadValue<Vector2>());
            _rb.AddForce(vec * moveSpeed);
            
            // _rb.v
        }

        private void KeyboardMove(InputAction.CallbackContext ctx)
        {
            var vec = MoveUtilities.Map2DControlVectorTo3D(ctx.ReadValue<Vector2>());
            _rb.AddForce(vec * moveSpeed);
        }

        private void JoystickMove(InputAction.CallbackContext ctx)
        {
            var vec = MoveUtilities.Map2DControlVectorTo3D(ctx.ReadValue<Vector2>());
            _rb.AddForce(vec * moveSpeed);
        }
    }
}
