﻿using UnityEngine;

namespace Utilities
{
    public static class MoveUtilities
    {
        public static Vector3 Map2DControlVectorTo3D(Vector2 vec) => new Vector3(vec.x, 0, vec.y);
        public static Vector3 Map2DControlVectorTo3D(Vector3 vec) => new Vector3(vec.x, 0, vec.y);
    }
}